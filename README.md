# Sbt Standard Libraries

This is a collection of libraries most commonly used at Shoplane.
The purpose is to bring uniformity in version and the libraries
used across services.

Hence this repo will be primarily used to keep up with the latest versions.